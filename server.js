const express = require('express')
const connectDb = require('./config/db')
const app = express()

// Connect to mongoDb
connectDb()

// Middleware
app.use(express.json({ extended: false }))

// Welcome (End-Point)
app.get('/', (req, res) =>
  res.json({ msg: 'Pugee Contacts API' })
)

// Store Routers File
const UsersRouter = require('./routers/users')
const AuthRouter = require('./routers/auth')
const ContactsRouter = require('./routers/contacts')

// Define Routers
app.use('/api/users', UsersRouter)
app.use('/api/auth', AuthRouter)
app.use('/api/contacts', ContactsRouter)

// Start Server
const PORT = process.env.PORT || 5000
app.listen(
  PORT, () => console.log(`Server started on port ${PORT}`)
)