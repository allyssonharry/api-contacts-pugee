import React, { useContext, useEffect, useState } from 'react'
import ContactContext from '../../context/contact/contactContext'

const ContactForm = () => {
  const contactContext = useContext(ContactContext)

  const { addContact, current, clearCurrent, updateContact } = contactContext

  useEffect(() => {
    if (current === null) {
      setContact({
        name: '',
        email: '',
        phone: '',
        type: 'personal'
      })
    } else {
      setContact(current)
    }
  }, [contactContext, current])

  const [contact, setContact] = useState({
    name: '', email: '', phone: '', type: 'personal'
  })

  const { name, email, phone, type } = contact

  const onChange = e => {
    setContact({ ...contact, [e.target.name]: e.target.value })
  }
  const onSubmit = e => {
    e.preventDefault()

    //contactContext.addContact(contact)

    if (current === null) {
      addContact(contact)
    } else {
      updateContact(contact)
    }

    setContact({
      name: '',
      email: '',
      phone: '',
      type: 'personal'
    })
  }
  const clearAll = () => {
    clearCurrent()
  }

  return (
    <ContactContext.Provider
      value={{}}
    >
      <form onSubmit={onSubmit}>
        <h2>{current ? 'Editar contato' : 'Cadastrar contato'}</h2>
        <input
          type='text'
          name='name'
          placeholder='Nome'
          value={name}
          onChange={onChange}
        />

        <input
          type='text'
          name='email'
          placeholder='E-mail'
          value={email}
          onChange={onChange}
        />

        <input
          type='text'
          name='phone'
          placeholder='Telefone'
          value={phone}
          onChange={onChange}
        />

        <h5>Tipo</h5>
        <label>
          <input
            type='radio'
            name='type'
            value='professional'
            checked={type === 'professional' ? 'checked' : null}
            onChange={onChange}
          /> Profissional
        </label>
        <label>
          <input
            type='radio'
            name='type'
            value='personal'
            checked={type === 'personal' ? 'checked' : null}
            onChange={onChange}
          /> Pessoal
        </label>

        <button type='submit'>{current ? 'Atualizar' : 'Cadastrar'}</button>

        {current && (
          <button onClick={clearAll}>Clear</button>
        )}
      </form>
    </ContactContext.Provider>
  )
}

export default ContactForm