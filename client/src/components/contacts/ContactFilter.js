import React, { useContext, useEffect, useRef } from 'react'
import ContactContext from '../../context/contact/contactContext'

const ContactFilter = () => {
  // use Context
  const contactContext = useContext(ContactContext)
  const text = useRef('')

  const { filtered } = contactContext

  useEffect(() => {
    if (filtered === null) text.current.value = ''
  })

  // input Changes
  const onChange = event => {
    if (text.current.value !== '') {
      contactContext.filterContacts(event.target.value)
      console.log(contactContext)
    } else {
      contactContext.clearFilter()
    }
  }

  return (
    <>
      <form>
        <input ref={text} type='text' placeholder='Filter contacts...' onChange={onChange} />
      </form>
    </>
  )
}

export default ContactFilter