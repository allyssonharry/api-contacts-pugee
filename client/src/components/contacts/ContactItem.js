import React, { useContext } from 'react'
import PropTypes from 'prop-types'
import ContactContext from '../../context/contact/contactContext'
import styled from 'styled-components'
import { Mail, Phone } from 'react-feather'

const Card = styled.div`
  background-color: #fff;
  box-shadow: 1px 2px 3px #ddd;
  margin-bottom: 1rem;
  padding: 0.45rem;
`
const CardTitle = styled.h3`
  display: inline-block;
`
const CardBadge = styled.span`
  font-size: 11px;
  font-weight: bold;
  color: blue;
  text-transform: uppercase;
  vertical-align: middle
`
const CardContent = styled.div`
  display: flex;
  flex-direction: column;
`

const ContactItem = ({ contact }) => {
  const contactContext = useContext(ContactContext)
  const { setCurrent, deleteContact, clearCurrent } = contactContext

  const { id, name, email, phone, type } = contact // props

  const onDelete = () => {
    deleteContact(id)
    //return window.alert(id)
    clearCurrent()
  }

  const badgeType = 'badge ' + (type === 'professional' ? 'badge-success' : 'badge-primary')

  return (
    <>
      <Card>
        <CardTitle>
          {name}{' '} <CardBadge className={badgeType}>{type}</CardBadge>
        </CardTitle>
        <CardContent>
          {email && (<div><Mail size={14} /> {email}</div>)}
          {phone && (<div><Phone size={14} /> {phone}</div>)}
          <div>
            <span><button onClick={() => setCurrent(contact)}>Edit</button></span>
            <span><button onClick={onDelete}>Apagar</button></span>
          </div>
        </CardContent>
      </Card>
    </>
  )
}

ContactItem.propTypes = {
  contact: PropTypes.object.isRequired
}

export default ContactItem