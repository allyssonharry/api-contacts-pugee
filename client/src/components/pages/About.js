import React from 'react'
import styled from 'styled-components'
import media from 'styled-media-query'

const Box = styled.div`
  max-width: 1010px;
  margin: 0 auto;
  padding: 0;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  ${media.lessThan('medium')`
    padding: 0 .75rem;
  `}
`
const About = () => {
  return (
    <Box>
      <h1>Contatos</h1>
    </Box>
  )
}

export default About