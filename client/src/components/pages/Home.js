import React from 'react'
import Contacts from '../contacts/Contacts'
import ContactForm from '../contacts/ContactForm'
import ContactFilter from '../contacts/ContactFilter'
import styled from 'styled-components'
import media from 'styled-media-query'

const Box = styled.div`
  max-width: 1010px;
  margin: 0 auto;
  padding: 0;
  ${media.lessThan('medium')`
    padding: 0 .75rem;
  `}
`

const Home = () => {
  return (
    <>
      <Box>
        <ContactFilter />
      </Box>
      <Box>
        {/* Contact Form */}
        <ContactForm />
      </Box>
      <Box>
        <Contacts />
      </Box>
    </>
  )
}

export default Home