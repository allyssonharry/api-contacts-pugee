import React, { useState } from 'react'

const Login = () => {

  const [user, setUser] = useState({
    email: '',
    password: ''
  })

  // Destruct
  const { email, password } = user

  const onChange = e => setUser({
    ...user,
    [e.target.name]: e.target.value
  })

  const onSubmit = e => {
    e.preventDefault()
    console.log('Login submit')
  }

  return (
    <>
      <div className='form-container'>
        <h2>Register</h2>
        <form className='form-register'>

          <div className='form-group'>
            <label htmlFor='email'>E-mail</label>
            <input type='text' name='email' value={email} onChange={onChange} />
          </div>

          <div className='form-group'>
            <label htmlFor='password'>Senha</label>
            <input type='password' name='password' value={password} onChange={onChange} />
          </div>

          <input type='submit' value='Registrar' onClick={onSubmit} />
        </form>
      </div>
    </>
  )
}

export default Login