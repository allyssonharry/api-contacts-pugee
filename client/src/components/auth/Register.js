import React, { useContext, useEffect, useState } from 'react'
import AlertContext from '../../context/alert/alertContext'
import AuthContext from '../../context/auth/authContext'

const Register = () => {
  // Alert Context
  const alertContext = useContext(AlertContext)
  const authContext = useContext(AuthContext)
  const { setAlert } = alertContext

  const { register, error, clearErrors } = authContext

  useEffect(() => {
    if (error === 'Usuário já cadastrado.') {
      setAlert(error, 'danger')
      clearErrors()
    }
  }, [error])

  const [user, setUser] = useState({
    name: '',
    email: '',
    password: '',
    password2: ''
  })

  // Destruct
  const { name, email, password, password2 } = user

  const onChange = e => setUser({
    ...user,
    [e.target.name]: e.target.value
  })

  const onSubmit = e => {
    e.preventDefault() // no-validation form !
    if (name === '' || email === '' || password === '') {
      setAlert('Por favor, preencha os campos com (*)', 'danger')
    } else if (password !== password2) {
      setAlert('Desculpe, a senha não confere.', 'danger')
    } else {
      console.log('Registra submit')
      register({ name, email, password })
    }
  }

  return (
    <>
      <div className='form-container'>
        <h2>Register</h2>
        <form className='form-register'>
          <div className='form-group'>
            <label htmlFor='name'>Nome *</label>
            <input type='text' name='name' value={name} onChange={onChange} required />
          </div>

          <div className='form-group'>
            <label htmlFor='email'>E-mail *</label>
            <input type='email' name='email' value={email} onChange={onChange} required />
          </div>

          <div className='form-group'>
            <label htmlFor='password'>Senha *</label>
            <input type='password' name='password' minLength='5' value={password} onChange={onChange} required />
          </div>

          <div className='form-group'>
            <label htmlFor='password2'>Confirmar Senha *</label>
            <input type='password' name='password2' minLength='5' value={password2} onChange={onChange} required />
          </div>

          <input type='submit' value='Registrar' onClick={onSubmit} />
        </form>
      </div>
    </>
  )
}

export default Register