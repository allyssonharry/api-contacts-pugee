import React, { Fragment } from 'react'
import { Link, withRouter } from 'react-router-dom'
import PropTypes from 'prop-types'
import { Briefcase, Home } from 'react-feather'
import styled from 'styled-components'

// Header Styles Component
const HeaderContent = styled.div`
  max-width: 1010px;
  margin: 0 auto;
  padding: 1rem;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`
const Logo = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
`
const Title = styled.h3`
  margin: 0 0 0 .45rem;
  font-weight: bold;
`
const Links = styled(Logo)`
  > a {
    color: #fff;
    font-weight: bold;
    margin: 0 .75rem;
    text-decoration: none;
    &:hover {
      color: #ffd65a;
    }
    &.active {
      color: #ffd65a;
    }
  }
`

const Navbar = ({ icon, title, location }) => {
  return (
    <Fragment>
      <HeaderContent>
        <Logo>{icon} <Title>{title}</Title></Logo>
        <Links>
          <Link to='/' className={location.pathname === '/' ? 'active' : null}><Home size={18} /></Link>
          <Link to='/about' className={location.pathname.startsWith('/about') ? 'active' : null}>Sobre</Link>
          <Link to='/login' className={location.pathname.startsWith('/login') ? 'active' : null}>Entrar</Link>
          <Link to='/register' className={location.pathname.startsWith('/register') ? 'active' : null}>Registrar-se</Link>
        </Links>
      </HeaderContent>
    </Fragment>
  )
}

Navbar.defaultProps = {
  icon: <Briefcase size={26} />,
  title: 'Contatos'
}
Navbar.propTypes = {
  icon: PropTypes.object.isRequired,
  title: PropTypes.string.isRequired
}

export default withRouter(Navbar)