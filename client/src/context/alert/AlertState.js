import React, { useReducer } from 'react'
import uuid from 'uuid'
import AlertContext from './alertContext'
import alertReducer from './alertReducer'
import { REMOVE_ALERT, SET_ALERT } from '../types'

const AuthState = ({ children }) => {
  // Initial defaults
  const initialState = []

  // Set alert
  const setAlert = (msg, type) => {
    const id = uuid.v4()

    dispatch({
      type: SET_ALERT,
      payload: { id, msg, type }
    })

    setTimeout(() => dispatch({ type: REMOVE_ALERT, payload: id }), 5000)
  }

  const [state, dispatch] = useReducer(alertReducer, initialState)

  return (
    <AlertContext.Provider
      value={{
        alerts: state,
        setAlert
      }}
    >
      {children}
    </AlertContext.Provider>
  )
}

export default AuthState
