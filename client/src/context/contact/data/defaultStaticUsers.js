const contacts = [
  {
    id: 1,
    name: 'Allysson',
    email: 'allysson@email.com',
    phone: '0000-0000',
    type: 'personal'
  },
  {
    id: 2,
    name: 'Erica',
    email: 'erica@email.com',
    phone: '0000-0000',
    type: 'professional'
  },
  {
    id: 3,
    name: 'Gustavo',
    email: 'gustavo@email.com',
    phone: '0000-0000',
    type: 'personal'
  }
]

export default contacts