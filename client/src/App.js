import React, { Component, Fragment } from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import styled from 'styled-components'
import Navbar from './components/Navbar'
import Home from './components/pages/Home'
import About from './components/pages/About'
import { Briefcase } from 'react-feather'
import AlertState from './context/alert/AlertState'
import Alerts from './components/Alerts'
import ContactState from './context/contact/ContactState'
import AuthState from './context/auth/AuthState'
import Register from './components/auth/Register'
import Login from './components/auth/Login'

// Styles Component
const Header = styled.header`
  background-color: black;
  color: #fff;
`

class App extends Component {
  render () {
    return (
      <>
        <AuthState>
          <ContactState>
            <AlertState>
              <Router>
                <Fragment>
                  <Header>
                    <Navbar icon={<Briefcase size={22} />} title='Contatos' />
                  </Header>
                  <Alerts />
                  <Switch>
                    <Route exact path='/' component={Home} />
                    <Route exat path='/about' component={About} />
                    <Route exat path='/login' component={Login} />
                    <Route exat path='/register' component={Register} />
                  </Switch>
                </Fragment>
              </Router>
            </AlertState>
          </ContactState>
        </AuthState>
      </>
    )
  }
}

export default App
