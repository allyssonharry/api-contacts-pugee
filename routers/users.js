const express = require('express')
const router = express.Router()
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const config = require('config')
const { check, validationResult } = require('express-validator')

// User Schema
const User = require('../models/User')

/**
 * Retrieve all users
 *
 * @route     GET api/users
 * @desc      Retrieve all users
 * @access    Public
 */
router.get('/', (req, res) => {
  const {
    user
  } = req.body
  res.json({ user })
})

/**
 * @route     POST api/users
 * @desc      Register a new user
 * @access    Public
 */
router.post('/', [
    // Validate fields
    check('name', 'Informe seu nome').not().isEmpty(),
    check('email', 'Informe um e-mail válido').isEmail(),
    check('password', 'A senha deve ter 4 ou mais caracteres').isLength({ min: 4 })
  ],
  async (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) return res.status(400).json({ errors: errors.array() })

    const {
      name,
      email,
      password
    } = req.body

    try {
      // Check if user already exists
      let user
      if (await User.findOne({ email })) {
        return res.status(400).json({ msg: 'Usuário já cadastrado!' })
      }

      user = new User({
        name,
        email,
        password
      })

      const salt = await bcrypt.genSalt(10)
      user.password = await bcrypt.hash(password, salt)

      await user.save()

      const payload = {
        user: {
          id: user.id
        }
      }

      // Generate the token
      jwt.sign(payload, config.get('jwtSecret'), {
        expiresIn: 360000,
      }, (err, token) => {
        if (err) {
          throw err
        }
        res.json({ token })
      })
    } catch (err) {
      console.log(err.message)
      res.status(500).send('500 - Server error.')
    }
  }
)

module.exports = router