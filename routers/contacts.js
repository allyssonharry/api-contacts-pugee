const express = require('express')
const router = express.Router()
const { check, validationResult } = require('express-validator')

// Middleware
const auth = require('../middleware/auth')

// Schemas
//const User = require("../models/User");
const Contact = require('../models/Contacts')

/**
 * @route   GET api/contacts
 * @desc    Get all users contacts
 * @access  Private
 */
router.get('/', auth, async function (req, res) {
  try {
    const contacts = await Contact.find({ user: req.user.id }).sort({
      date: -1
    })
    await res.json(contacts)
  } catch (err) {
    console.log(err.message)
    res.status(500).send('500 - Server error.')
  }
})

// @route   POST api/contacts
// @desc    Add new contacts
// @access  Private
router.post('/', [
    check('name', 'Digite um nome...').not().isEmpty()
  ],
  async (req, res) => {

    const errors = validationResult(req)
    if (!errors) {
      return res.status(400).json({ error: errors.array() })
    }

    const { name, email, phone, type } = req.body

    try {
      // Verifica se o contato já existe
      if (await Contact.findOne({ email })) {
        return res.status(400).json({ msg: 'Contato já cadastrado!' })
      }

      // ID do usuário
      let userId = req.user.id

      const newContact = new Contact({
        name,
        email,
        phone,
        type,
        user: userId
      })

      const contact = await newContact.save()

      await res.json(contact)
    } catch (error) {
      console.log(error.message)
      res.status(500).send('500 - Server error.')
    }
  }
)

// @route   PUT api/contacts/:id
// @desc    Update contact
// @access  Private
router.put('/:id', (req, res) => res.send('Update contact'))

// @route   DELETE api/contacts/:id
// @desc    Delete contact
// @access  Private
router.delete('/:id', (req, res) => res.send('Delete contact'))

module.exports = router
