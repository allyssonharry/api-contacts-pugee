const express = require('express')
const router = express.Router()
const jwt = require('jsonwebtoken')
const bcrypt = require('bcryptjs')
const { check, validationResult } = require('express-validator')
// Init config
const config = require('config')
// Middleware
const auth = require('../middleware/auth')
// User Schema
const User = require('../models/User')

/**
 * Get logged in user
 *
 * @route     GET api/auth
 * @desc      Get logged in user
 * @access    Private
 */
router.get('/', auth, async (req, res) => {
  try {
    const user = await User.findById(req.user.id).select('-password')
    await res.json(user)
  } catch (err) {
    console.log(err.message)
    res.status(500).send('500 - Server error.')
  }
})

/**
 * Authenticate user
 *
 * @route     POST api/auth
 * @desc      Authenticate user & get token
 * @access    Public
 */
router.post('/', [
    check('email', 'Digite um e-mail válido').isEmail(),
    check('password', 'Digite sua senha').exists()
  ], async (req, res) => {
    const errors = validationResult(req)
    if (!errors) {
      return res.status(500).json({ errors: errors.array() })
    }

    const {
      email,
      password
    } = req.body

    try {
      // Check if user already exists
      let user = await User.findOne({ email })
      if (!user) {
        return res.status(400).json({ msg: 'Credênciais inválida(s)' })
      }

      // Check if password match
      let isMatch = await bcrypt.compare(password, user.password)
      if (!isMatch) {
        return res.status(400).json({ msg: 'Credênciais inválida(s)' })
      }

      const payload = {
        user: {
          id: user.id
        }
      }

      jwt.sign(
        payload,
        config.get('jwtSecret'),
        {
          expiresIn: 360000,
        },
        (err, token) => {
          if (err) {
            throw err
          }
          res.json({ token })
        }
      )
    } catch (err) {
      console.log(err.message)
      res.status(500).send('500 - Server error.')
    }
  }
)

module.exports = router