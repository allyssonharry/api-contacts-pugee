const mongoose = require("mongoose");
const config = require("config");
const database = config.get("mongoURI");

module.exports = connectDb = async () => {
  try {
    await mongoose.connect(database, {
      useNewUrlParser: true,
      useCreateIndex: true,
      useFindAndModify: false
    });
    console.log("MongoDB Conectado.");
  } catch (err) {
    console.error(err.message);
    process.exit(1);
  }
};
